
const express = require('express')
const router = express.Router()

// The following endpoints are supported.
//
// /before_receive
// /route
// /before_route (only for PROTOCOL=ROUTE connectors)
// /before_send
// /after_send
// /before_dlr (gets dlrinfo)
// /after_dlr

router.post('/before_receive', (req, res) => {
	console.log('in before_receive, body:', req.body)
	const qe = req.body.qe
	let respbuf = ''
	if (qe.DESTADDR === '911')  respbuf = 'result = 8'
	else respbuf = 'qe.SMPPOPTION=0x1400:414243\nqe.SMPPOPTION=0x1401:61626364'
	res.send(respbuf)
})

router.post('/route', (req, res) => {
	console.log('in route') //, qe:', req.body.qe)
	const qe = req.body.qe
	let respbuf = ''
	if (qe.DESTADDR === '123')
		respbuf = Buffer.from('route = out')
	res.send(respbuf)
})

router.post('/before_dlr', (req, res) => {
	console.log('in before_dlr, body:', req.body)
	res.send('dlrinfo.mcc=240\ndlrinfo.mnc=05')
})

router.use((req, res, next) => {
	console.log('nothing to do for %s %s', req.method, req.url)
	res.status(404).end()
})

module.exports = router
