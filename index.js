
const express = require('express')
const app = express()

// if the content-type is application/json, the incoming POST body is parsed automatically
app.use(express.json());

// connect the plugin(s)
app.use('/plugin', require('./plugin'))
app.use('/plugin2', require('./plugin2'))
app.use('/credits', require('./credits'))

// catch 404 and forward to error handler
const createError = require('http-errors')
app.use(function(req, res, next) { next(createError(404)) })

module.exports = app
