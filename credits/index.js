
const express = require('express')
const router = express.Router()

// The following endpoints are supported.
//
// /update - json fields: userid, column, and mods

router.post('/update', (req, res) => {
    console.log('in update, body:', req.body)
    let respbuf = ''
    res.send(respbuf)
})

router.use((req, res, next) => {
    console.log('nothing to do for %s %s', req.method, req.url)
    res.status(404).end()
})

module.exports = router
