
const express = require('express')
const router = express.Router()

var counter = 0;

router.post('/before_receive', (req, res) => {
	const qe = req.body.qe
	counter = counter + 1
	res.send('qe.MESSAGE=Message ' + counter + ': ' + qe.MESSAGE)
})

router.use((req, res, next) => {
	res.status(404).end()
})

module.exports = router

